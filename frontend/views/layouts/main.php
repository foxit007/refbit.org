<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\criptCurrency\Currency;
use frontend\widgets\menuHeder\MenuHeder;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body >
        <?php $this->beginBody() ?>

        <div class="wrap" style="background-color: #f5f5f5">
            <?php
            NavBar::begin([
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                    'style' => 'font-size: 16px; background-color:#00010A;',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/bitcoin/faucet']],
       
               // ['label' => 'Ethereum', 'url' => ['/bitcoin/ethereum']],
            ];
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'dropDownCaret' => '',
            ]);
            NavBar::end();
            ?>

            <div class="container" style="background-color: #FBFBFB">
                <div class="col-xs-12 col-md-8">
            <?php
                $items = [
                    ['label' => 'Bitcoin краны', 'url' => '/bitcoin/faucet'],
                    //['label' => 'Bitcoin игры', 'url' => '/bitcoin/game'],
                   // ['label' => 'Облачный майнинг', 'url' => '/bitcoin/cloud-maining'],
                   // ['label' => 'Обмен Bitcoin', 'url' => '/bitcoin/exchange-bitcoin'],
                ];
                echo MenuHeder::widget(['arrayMenu' => $items]);
            ?>
            <br><br><br>
            <?= $content ?>
            </div>
                <div class="col-xs-12 col-md-4">
                    <?= Currency::widget(); ?>
                </div>
            </div>
        </div>
        <footer class="footer" style="">
            <div class="container">
                <p class="pull-left">&copy; Refbit <?= date('Y') ?></p>

                <p class="pull-right">Курсы предоставленны с сайта <?=Html::a('Bittrex','http://www.bittrex.com' ) ?></p>
            </div>
        </footer>
        
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
