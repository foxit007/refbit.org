    <?php
        use yii\grid\GridView;
    ?>   
   
   <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'table table-hover table-bordered table-striped'],
            'summary' => '',
            'columns' => [
                ['label' => 'Краны', 'attribute' => 'crane',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center']],
                ['label' => 'Интервал', 'attribute' => 'interval',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center']],
                ['label' => 'Выплата сатоши', 'attribute' => 'satoshi',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center']],
                ['label' => 'Минимальный вывод сатоши', 'attribute' => 'min_satoshi_out',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center']],
            ],
        ])
           
    ?>

 
