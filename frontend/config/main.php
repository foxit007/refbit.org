<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        
           'cache' => [   
            'class' => 'yii\caching\FileCache',
            'cachePath' => '/var/www/console/runtime/cache',
        ],

        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,   // Disable r= routes
            'showScriptName' => false,   // Disable index.php
            'enableStrictParsing' => false,
            'rules' => [
                //'faucet'=>'bitcoin/faucet',
                ''=>'bitcoin/faucet',
                //'<action:(.*)>' => 'bitcoin/<action>',
               
            ],
        ],
        
    ],
    'params' => $params,
    'defaultRoute' =>'bitcoin/faucet',
];
