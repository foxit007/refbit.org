<?php
namespace frontend\controllers;

use yii\web\Controller;
use console\models\Bittrex;
use yii\data\ActiveDataProvider;
use frontend\models\Bitcoin;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use Yii;

class BitcoinController extends Controller
{

   public function actionFaucet() 
    {
       $query = new Query;
     
        $dataProvider= new ArrayDataProvider([
           'allModels' =>$query->from(Bitcoin::tableName())->all(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('faucet',[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionGame()
    {
        return $this->render('game');
    }        
    
    public function actionCloudMaining()
    {
        return $this->render('cloud-maining');
    }  
    
    public function actionExchangeBitcoin()
    {
        return $this->render('exchange-bitcoin');
    }        
    
}