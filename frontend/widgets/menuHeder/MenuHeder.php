<?php

namespace frontend\widgets\menuHeder;
use yii\base\Widget;
/**
 * Description of menuHeder
 * @author foxit
 */
class MenuHeder extends Widget 
{
    
    public $arrayMenu=null;
    
    public function run()
    {
        return $this->render('menu',[
            'arrayMenu'=>$this->arrayMenu,
        ]);
    }
}
