<?php 
    use yii\widgets\Pjax;
    use yii\helpers\Html;
    
?>

    <div class="text-center"  style="background-color: #337ab7; border-radius: 0px; color: white; padding: 10px 12px; font-size: 16px;">
        Курс криптовалют
    </div>
    <br>
    <div class="text-justify">
        <?php Pjax::begin(['timeout' => 20000]); ?> 
        <table class="table table-bordered table-responsive" style="text-align: center;" >
            <thead style="text-align: center;">
                <tr>
                    <th style="text-align: center;">Валюта</th>
                    <th style="text-align: center;">Курс в BTC</th>
                    <th style="text-align: center;">Курс в $</th>
                </tr>
            </thead>
            <?php
          $i =0;
   
            //$sliced = array_slice($list, 0);
            foreach ($list as $value) {
                //  cho '<pre>'; echo ($value);die;
           
                echo '<tr>';
                echo '<td>';
                
                echo $market[$i];
                
                echo '</td>';
                echo '<td >';
                if($i==0){
                    echo 1;
                }else{
                    echo Yii::$app->formatter->format($value['result']['Last'], ['decimal', 8]);
                }
                echo '</td>';
                echo '<td >';
                if($i==0){
                   echo Yii::$app->formatter->format($value['result']['Last'], ['decimal', 2]); 
                } else {
                   echo Yii::$app->formatter->format($value['result']['Last'] * $btcusd, ['decimal', 2]);
                }
                echo '</td>';
                echo '</tr>';
                $i++;
                if ($i>=10) break;
            }
            ?>
            <?= Html::a("Обновить", [''], ['class' => 'btn btn-lg btn-primary hidden', 'id' => 'refreshButton']) ?>
        </table>
            <?php Pjax::end(); ?>
    </div>


<?php 
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#refreshButton").click(); }, 10000);
});
JS;
$this->registerJs($script);
?>

