<?php

namespace frontend\widgets\criptCurrency;
use yii\base\Widget;
use console\models\Bittrex;
use console\controllers\BittrexController;
use Yii;
/**
 * Description of Currency
 * @author foxit
 */
class Currency extends  Widget  
{
    public $market=['USDT-BTC','BTC-ETH','BTC-LTC',
                    'BTC-XRP','BTC-ZEC','BTC-DCR',
                    'BTC-XMR','BTC-DASH','BTC-DOGE',
                    'BTC-SIB'];
    public $list=null;
    
    public function run()
    {
        if(!Yii::$app->cache->exists("bittrex")){
            $this->list= Bittrex::getTicker($this->market);
        }else{ 
    	$this->list=Yii::$app->cache->get("bittrex");
        }
        $btcusd=$this->list[0]['result']['Last'];
        return $this->render('table',[
            'list'=>$this->list,
            'market'=>$this->market,
            'btcusd'=>$btcusd,
        ]);
    }
}
