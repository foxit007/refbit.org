<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bitcoin`.
 */
class m170710_121023_create_bitcoin_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bitcoin', [
            'id' => $this->primaryKey(),
            'crane'=>$this->string(),
            'interval'=>$this->integer(),
            'satoshi'=>$this->integer(),
            'min_satoshi_out'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bitcoin');
    }
}
