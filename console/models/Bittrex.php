<?php

namespace console\models;

/**
 * Description of Biittrex
 * @author foxit
 */
class Bittrex {
    
    private static $apikey='f9e26d856ae4423889645054a91101ef';
    private static $apisecret ='1f9e26d856fghfgh785ae4423889645054a91101ef';

    
    private static function connect($uri) 
    {
        
        $nonce = time();
        $uri =$uri . '&apikey='. self::$apikey . '&nonce=' . $nonce;
        $sign = hash_hmac('sha512', $uri, self::$apisecret);
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:' . $sign));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return $ch;
    }

    private static function  processing($ch)
    {
        
        $execResult=curl_exec($ch);
        $arrData = json_decode($execResult,true); 
        return $arrData;  
    }

    public static function getTicker($market)
    {
        $arrData; 
        foreach ($market as $key ) {
            $arrData[]=self::processing(self::connect('https://bittrex.com/api/v1.1/public/getticker?'. 'market=' . $key)); 
        }
    
        return $arrData;
    }

}
