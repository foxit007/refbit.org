<?php

namespace console\controllers;
use yii\console\Controller;
use console\models\Bittrex;
use Yii;

/**
 * @author foxit
 */

class BittrexController extends Controller {
    
     public $market=['USDT-BTC','BTC-ETH','BTC-LTC',
                    'BTC-XRP','BTC-ZEC','BTC-DCR',
                    'BTC-XMR','BTC-DASH','BTC-DOGE',
                    'BTC-SIB','BTC-XDN'];
     
    public function actionTicker()
    {
        $currency =['BTC-LTC','BTC-SIB'];
         Bittrex::getTicker($currency);
    }
    
    public function actionCache()
    {
        $key = "bittrex";
        if (Yii::$app->cache->exists($key)){
         Yii::$app->cache->delete($key);   
        }
        Yii::$app->cache->add($key, Bittrex::getTicker($this->market));
        
    }        
}
